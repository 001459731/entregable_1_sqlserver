# ENTREGABLE_1_SQLSERVER

# 1. PASO 
~~~
-- Procedimiento almacenado para insertar una nueva venta 
CREATE PROCEDURE InsertarVenta
    @fecha DATE,
    @idcliente INT,
    @total DECIMAL
AS
BEGIN
    INSERT INTO venta (fecha, idcliente, total)
    VALUES (@fecha, @idcliente, @total);
END;
GO
~~~
# 2. PASO 
~~~
-- Procedimiento almacenado para actualizar una venta 
CREATE PROCEDURE ActualizarVenta
    @id INT,
    @fecha DATE,
    @id_cliente INT,
    @total DECIMAL(10, 2)
AS
BEGIN
    UPDATE venta
    SET fecha = @fecha, id_cliente = @id_cliente, total = @total
~~~
# 3. PASO 
-- Procedimiento almacenado para eliminar una venta 
CREATE PROCEDURE EliminarVenta
    @id INT
AS
BEGIN
    DELETE FROM venta WHERE id = @id;
END;
GO
# 4. PASO
~~~
drop PROCEDURE InsertarVenta;


-- Ejemplo de llamada al procedimiento InsertarVenta con un ID de usuario específico
EXEC InsertarVenta @fecha = '2024-06-12', @id_cliente = 1, @idusuario = 1, @total = 100.00;


-- Ejemplo de llamada al procedimiento ActualizarCategoria en SQL Server
EXEC ActualizarCategoria @idcategoria = 1, @nombre = 'Electrónicos', @descripcion = 'Productos electrónicos variados', @estado = 1;

-- Ejemplo de llamada al procedimiento EliminarCategoria en SQL Server
EXEC EliminarCategoria @idcategoria = 2;
~~~
# 5 CONTULAS Y ALGUNAS AGREGACIONES
~~~
select * from Venta;

CREATE PROCEDURE IngresarRegistro
AS
BEGIN
    BEGIN TRANSACTION;

    INSERT INTO tabla (columna1, columna2, ...)
    VALUES (valor1, valor2, ...);

    COMMIT TRANSACTION;
END;
GO


CREATE PROCEDURE ProcedimientoConControlDeErrores
AS
BEGIN
    BEGIN TRY
        BEGIN TRANSACTION;

        -- Comandos DML o cualquier operación que pueda generar errores

        COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
    END CATCH;
END;
GO

ALTER TABLE articulo
ADD CONSTRAINT CK_PrecioPositivo CHECK (precio > 0);

ALTER TABLE articulo
ADD CONSTRAINT DF_Stock DEFAULT 0 FOR stock;

ALTER TABLE articulo
ADD CONSTRAINT UQ_Codigo UNIQUE (codigo);

ALTER TABLE articulo
ADD CONSTRAINT PK_Articulo PRIMARY KEY (idarticulo);

CREATE PROCEDURE ObtenerDatosArticulo
    @idarticulo INT,
    @nombre VARCHAR(100) OUTPUT,
    @precio DECIMAL(10, 2) OUTPUT
AS
BEGIN
    SELECT @nombre = nombre, @precio = precio
    FROM articulo
    WHERE idarticulo = @idarticulo;
END;
GO

CREATE FUNCTION CalcularTotalVenta
    (@idventa INT)
RETURNS DECIMAL(10, 2)
AS
BEGIN
    DECLARE @total DECIMAL(10, 2);

    SELECT @total = SUM(precio * cantidad)
    FROM detalle_venta
    WHERE idventa = @idventa;

    RETURN @total;
END;
GO

SELECT c.nombre AS categoria, SUM(dv.precio * dv.cantidad) AS total_venta
FROM detalle_venta dv
INNER JOIN articulo a ON dv.idarticulo = a.idarticulo
INNER JOIN categoria c ON a.idcategoria = c.idcategoria
GROUP BY c.nombre;

CREATE VIEW VistaVentas AS
SELECT v.idventa, v.fecha, SUM(dv.precio * dv.cantidad) AS total_venta
FROM venta v
INNER JOIN detalle_venta dv ON v.idventa = dv.idventa
GROUP BY v.idventa, v.fecha;

CREATE VIEW VistaProductosAgotados AS
SELECT *
FROM articulo
WHERE stock = 0;
~~~

# PROCEDIMIENTO 2 
~~~
-- Procedimiento almacenado para insertar un nuevo usuario 
CREATE PROCEDURE InsertarUsuario
    @id_rol INT,
    @nombre_usuario VARCHAR(255),
    @password VARCHAR(255)
AS
BEGIN
    INSERT INTO usuario (id_rol, nombre_usuario, password)
    VALUES (@id_rol, @nombre_usuario, @password);
END;
GO
~~~
# PASO 1. 
~~~
-- Procedimiento almacenado para actualizar un usuario 
CREATE PROCEDURE ActualizarUsuario
    @id INT,
    @id_rol INT,
    @nombre_usuario VARCHAR(255),
    @password VARCHAR(255)
AS
BEGIN
    UPDATE usuario
    SET id_rol = @id_rol, nombre_usuario = @nombre_usuario, password = @password
    WHERE @id_rol = @id;
END;
GO
~~~
# PASO 2.
~~~
-- Procedimiento almacenado para eliminar un usuario 
CREATE PROCEDURE EliminarUsuario
    @id INT
AS
BEGIN
    DELETE FROM usuario WHERE id = @id;
END;
GO
~~~
# REALIZANDO LAS CONSULTAS
~~~
-- Ejemplo de llamada al procedimiento InsertarUsuario 
EXEC InsertarUsuario @id_rol = 1, @nombre_usuario = 'usuario1', @password = 'contraseña';

-- Ejemplo de llamada al procedimiento ActualizarUsuario 
EXEC ActualizarUsuario @id = 1, @id_rol = 2, @nombre_usuario = 'usuario_modificado', @password = 'nueva_contraseña';

-- Ejemplo de llamada al procedimiento EliminarUsuario en SQL Server
EXEC EliminarUsuario @id = 1;
~~~

# PROCEDIMIENTO 3

# paso 1. 
~~~
-- Procedimiento almacenado para insertar un nuevo rol 
CREATE PROCEDURE InsertarRol
    @nombre VARCHAR(255)
AS
BEGIN
    INSERT INTO rol (nombre)
    VALUES (@nombre);
END;
GO
~~~

# paso 2.
~~~

-- Procedimiento almacenado para actualizar un rol en SQL Server
CREATE PROCEDURE ActualizarRol
    @id INT,
    @nombre VARCHAR(255)
AS
BEGIN
    UPDATE rol
    SET nombre = @nombre
    WHERE id = @id; -- Asegúrate de usar el nombre correcto del campo de identificación
END;
GO
~~~



# paso 3. 
~~~

-- Procedimiento almacenado para eliminar un rol 
CREATE PROCEDURE EliminarRol
    @id INT
AS
BEGIN
    DELETE FROM rol WHERE id = @id;
END;
GO
~~~

# consultas

~~~

-- Ejemplo de llamada al procedimiento InsertarRol en SQL Server
EXEC InsertarRol @nombre = 'NuevoRol';


-- Ejemplo de llamada al procedimiento ActualizarRol en SQL Server
EXEC ActualizarRol @id = 1, @nombre = 'RolModificado';


-- Ejemplo de llamada al procedimiento EliminarRol en SQL Server
EXEC EliminarRol @id = 1;

~~~

# procedimiento 4 
# paso 1. 
~~~

-- Procedimiento almacenado para insertar una nueva persona 
CREATE PROCEDURE InsertarPersona
    @nombre VARCHAR(255),
    @tipo_persona VARCHAR(50),
    @tipo_documento VARCHAR(50),
    @num_documento VARCHAR(50), 
    @direccion VARCHAR(255),
    @telefono VARCHAR(50),
    @email VARCHAR(255)
~~~
# paso 2. 
~~~

EXEC InsertarPersona 
    @nombre = 'Juan',
    @tipo_persona = 'Cliente',
    @tipo_documento = 'DNI',
    @num_documento = '12345678',
    @direccion = 'Calle 123',
    @telefono = '555-1234',
    @email = 'juan@example.com';
~~~


# paso 3. 
~~~


-- Procedimiento almacenado para actualizar una persona
CREATE PROCEDURE ActualizarPersona
    @id INT,
    @nombre VARCHAR(255),
    @tipo_persona VARCHAR(50),
    @tipo_documento VARCHAR(50),
    @numero_documento VARCHAR(50),
    @direccion VARCHAR(255),
    @telefono VARCHAR(50),
    @email VARCHAR(255)
AS
~~~

# declarando una consulta alazar 
~~~

DECLARE @nombre VARCHAR(255) = 'Juan';
DECLARE @tipo_persona VARCHAR(50) = 'Cliente';
DECLARE @tipo_documento VARCHAR(50) = 'DNI';
DECLARE @numero_documento VARCHAR(50) = '12345678';
DECLARE @direccion VARCHAR(255) = 'Calle 123';
DECLARE @telefono VARCHAR(50) = '555-1234';
DECLARE @email VARCHAR(255) = 'juan@example.com';
DECLARE @id INT = 1; -- Supongamos que quieres actualizar la persona con ID 1
~~~



# consultas a los procedimiento 
~~~

-- Procedimiento almacenado para eliminar una persona 
CREATE PROCEDURE EliminarPersona
    @id INT
AS
BEGIN
    DELETE FROM persona WHERE id = @id;
END;
GO

EXEC EliminarPersona @id = 1;

~~~


# procedimiento 4

# paso 1 
~~~

-- Procedimiento almacenado para insertar un nuevo ingreso 
CREATE PROCEDURE InsertarIngreso
    @fecha DATE,
    @id_proveedor INT
AS
BEGIN
    INSERT INTO ingreso (fecha, id_proveedor)
    VALUES (@fecha, @id_proveedor);
END;
GO
~~~

# paso 2 
~~~

-- Procedimiento almacenado para insertar un nuevo ingreso 
CREATE PROCEDURE InsertarIngreso
    @fecha DATE,
    @id_proveedor INT
AS
BEGIN
    INSERT INTO ingreso (fecha, id_proveedor)
    VALUES (@fecha, @id_proveedor);
END;
GO
~~~

# paso 3 
~~~


-- Procedimiento almacenado para eliminar un ingreso 
CREATE PROCEDURE EliminarIngreso
    @id INT
AS
BEGIN
    DELETE FROM ingreso WHERE id = @id;
END;
GO
~~~
# realizando las consultas
~~~

EXEC InsertarIngreso @fecha = '2024-06-12', @id_proveedor = 1;
EXEC EliminarIngreso @id = 1;
~~~

